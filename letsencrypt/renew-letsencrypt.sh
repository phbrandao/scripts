#!/bin/bash

DOMAINS_FILE=/usr/share/letsencrypt/scripts/domain.txt
LOG_FILE=/var/log/renew-letsencrypt.log
DAYS_LEFT_TRIGGER=10

function log {
  echo `date` $1 $2 >> $LOG_FILE
}

function safeRunCommand {
   cmnd=$1

   $cmnd >> $LOG_FILE 2>&1

   if [ $? != 0 ]; then
      log ERREUR "Le script $0 s'est termine avec le code erreur $ERROR_CODE sur la commande $cmnd"
      exit $ERROR_CODE
   fi
}

log INFO "Debut du script $0 pour le renouvellement automatique de certificat Let's encrypt"

# On recupere le nombre de jours restants du certificat
DAYS_LEFT=`/usr/local/sbin/ssl-cert-check -b -f $DOMAINS_FILE |rev | sed -e 's/^[[:space:]]*//' | cut -d " " -f1 | rev`
DOMAIN=`cat $DOMAINS_FILE | cut -d " " -f1`

log INFO " Il reste $DAYS_LEFT jours de valide sur le certificat du domaine "$DOMAIN

if [ $DAYS_LEFT -lt $DAYS_LEFT_TRIGGER ]; then
  log INFO "Renouvellement du certificat..."
  safeRunCommand "/usr/sbin/ufw allow in 80"
  # Ajouter l'option --dry-run pendant les tests
  safeRunCommand "/usr/share/letsencrypt/bin/letsencrypt-auto renew"
  safeRunCommand "/bin/systemctl restart apache2.service"
  safeRunCommand "/usr/sbin/ufw deny in 80"
else
  log INFO "Il reste au moins $DAYS_LEFT_TRIGGER jours. Le certificat n'a pas besoin d'etre renouvele. Fin du script"
fi

log INFO "Le script $0 s'est termine avec succes"
